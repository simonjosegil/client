import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSnackBar,  MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition,} from '@angular/material/snack-bar';
import { LocalforageService, Item } from '../../services/localforage.service';
import { WebSocketService } from '../../services/web-socket.service';
import {Router} from '@angular/router'
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(
    private webService : WebSocketService, 
    private _snackBar: MatSnackBar,
    private localStorage: LocalforageService,
    private route:Router
    ) {  }
    
  ngOnInit(): void {

    setTimeout(()=>{
      this.localStorage.get('Environment').then((data:any) => {
        this.opcionEv = data;
        this.favoriteIM = this.opcionEv[3].method;
        this.mailIM = this.opcionEv[3].mail;
        this.passwordIM = this.opcionEv[3].password;
      })},300)

     setTimeout(()=>{ this.onLoad = this.errorTimeIni === false ? true : false; },30000) 
     setTimeout(()=>{ this.cont === 0 ? this.cont = this.guideArray.length : this.cont;  },30000)
     setTimeout(()=>{ this.timeCode = this.errorTimeIni === false ? true : false; },70000) 
 

     this.webService.resp$.subscribe( respuesta => {  
      if(respuesta[0].type == 2){
        this.message = this.opcionEv[1].active === false ? respuesta[2].ALERT : respuesta[2].ALERTA;  
        this.openSnackBar();
        this.PingValidate = respuesta[1].name === 'connected' ? true : false;
        this.errorTimeIni = respuesta[1].name === 'errorTimeIni' ? true : false;
        this.onLoad =  this.errorTimeIni === true ? false : true;
      }
      if(respuesta[1].name === 'connected'){
        this.localStorage.set('USER', respuesta[3].USER)
        this.onLoad = false;
        setTimeout(()=>{ this.route.navigate(['/chat', this.User.name]); },7000) 
      }
      if(respuesta[1].name === 'errorPing'){
        this.disableText=false;
        this.ping='';
      }
      }); 
  }

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  ping: any;
  msgError: any;
  errorValidateLogin = false;
  errorTimeIni = false;
  PingValidate = false;
  onLoad = false;
  cont = 0;
  esp = 0;
  timeCode = false;
  inputM = false;
  changeONav2 = false;


  userChat = {
    user: '',
    text: '', 
    type: 4,
    positionN: 0,
  }
  
  User = {
    name: '',
  }

  eventName = "send-message";
  data:any;
  blockNavAutoma: any;
  disableText=false;
  favoriteIM: any = '';
  IM: string[] = ['Default', '2', '3'];
  mailIM: any = '';
  passwordIM: any = '';


  guideArray = [
    {introText: "Welcome to Presales",
    introTextES: "Bienvenid@ a Presales",
    introTextBut: "Getting started",  
    introTextButES: "¿Primera vez en la APP?",
    introStyles: { 'width': '30%', 'margin': 'auto', 'margin-top': '5%', 'margin-left': '35%'}
    },
    {introText: "We start the step by step. Press continue",
    introTextES: "Iniciamos el paso a paso. Presione continúe",
    introTextBut: "Continue...",
    introTextButES: "Continúe...",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '-450px', 'margin-left': '15%'}
    },
    {introText: "This button displays the APP settings",
    introTextES: "Este botón despliega las configuraciones de la APP",
    introTextBut: "",
    introTextButES: "",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '-450px', 'margin-left': '15%'}
    },
    {introText: "Options to hide browser, change language and login",
    introTextES: "Opciones de ocultar navegador, cambiar idioma y login",
    introTextBut: "Continue...",
    introTextButES: "Continúe...",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '-450px', 'margin-left': '15%'}
    },
    {introText: "About the APP",
    introTextES: "Información y versión de la APP",
    introTextBut: "Continue...",
    introTextButES: "Continúe...",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '50px', 'margin-left': '5%'}
    },
    {introText: "Login with PingID",
    introTextES: "Inicie sesión con el PingID",
    introTextBut: "Continue...",
    introTextButES: "Continúe...",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '12px', 'margin-left': '65%'}
    },
    {introText: "¡Excellent, we have reached the end!",
    introTextES: "¡Excelente, hemos llegado al final!",
    introTextBut: "Continue...",
    introTextButES: "Continúe...",
    introStyles: { 'width': '420px', 'margin': 'auto', 'margin-top': '12px', 'margin-left': '65%'}
    }
  ]; 

  iM(){
    this.inputM = true;
    this.changeONav2 = true;
  }

introText = this.guideArray[0].introText;
introTextES = this.guideArray[0].introTextES;
introTextBut = this.guideArray[0].introTextBut;
introTextButES = this.guideArray[0].introTextButES;
introStyles =  this.guideArray[0].introStyles;


opcionEv: any;

changeONav = false;

flagIm(){
  this.changeONav2 = true;
  this.opcionEv[3].method = this.favoriteIM;
  this.opcionEv[3].mail = this.mailIM;
  this.opcionEv[3].password = this.passwordIM;
  setTimeout(()=>{this.opcionEv[3].method = this.favoriteIM;},50)
  setTimeout(()=>{this.opcionEvStatus();},200)
 }

flagEv(re:any){
 console.log(re);
  this.opcionEvStatus();
  if(re == 0){
    this.changeONav = true;
  }
}

opcionEvStatus(){
  setTimeout(()=>{
    this.localStorage.set('Environment', this.opcionEv);
    this.localStorage.set('USER', this.mailIM.trim());
    this.getEvStatus();
  },100)

}

getEvStatus(){
  setTimeout(()=>{
    this.localStorage.get('Environment').then((data:any) => {
        this.opcionEv =  data;
        console.log(this.opcionEv);
  })},100)
}

restore(){
this.localStorage.remove('opcionBom');
this.localStorage.remove('Environment');
this.message = 'Factory restored';  
this.openSnackBar();
setTimeout(()=>{ window.location.reload(); },3000) 

}

guide(){
  this.cont = this.cont +1;

  if(this.guideArray.length > this.cont){
    this.introText = this.guideArray[this.cont].introText;
    this.introTextES = this.guideArray[this.cont].introTextES;
    this.introTextBut = this.guideArray[this.cont].introTextBut;
    this.introTextButES = this.guideArray[this.cont].introTextButES;
    this.introStyles = this.guideArray[this.cont].introStyles;
  
  }else if (this.cont >= this.guideArray.length){
    this.cont = this.guideArray.length;
  }
}

content = [
    {contentText: "Config",
    contentTextES: "Configuración",
    },
    {contentText: "Enter PingID",
    contentTextES: "Ingrese el PingID",
    },
    {contentText: "Approve login from the PingID APP on your Smartphone",
    contentTextES: "Apruebe el ingreso desde la APP PingID en su Smartphone",
    },
    {contentText: "You can also enter PingID from your cell phone",
    contentTextES: "También puede ingresar el PingID desde su celular",
    },
    {contentText: "About Presales",
    contentTextES: "Acerca de Presales",
    },
    {contentText:  "Presales is a Framework that supports \n automated applications, such as the BOM \n app to consult both \n SKUs and ConfigID",
    contentTextES: "Presales es un Marco de Trabajo \n que soporta aplicaciones automatizadas,\n como la app BOM para consultar \n tanto SKU como ConfigID",
    },
    {contentText:  "version 2.0.0",
    contentTextES: "versión 2.0.0",
    },
    {contentText:  "You must enter 6 numeric characters",
    contentTextES: "Debe ingresar 6 caracteres numéricos",
    },
    {contentText:  "You must restart the APP to observe the changes",
    contentTextES: "Debe reiniciar la APP para observar los cambios",
    },
    {contentText:  "You can enter with the 6-digit code of the mobile APP",
    contentTextES: "Puede ingresar con el código de 6 dígitos de la APP movil",
    },
    {contentText:  "Input method",
    contentTextES: "Metodo de ingreso",
    },
    {contentText:  "Correo empresarial",
    contentTextES: "Enterprise mail",
    },
];

sendMSG(){
    if(this.ping.length === 6 && Number(this.ping) == this.ping){
      this.userChat.text = this.ping;
      this.userChat.text = this.ping;
      this.userChat.positionN = 6;
      this.webService.emit('send-message', this.userChat);
      this.userChat.positionN = 0;
      this.userChat.text = '';
      this.disableText=true;
      this.errorValidateLogin = false;

    }else{
     this.errorValidateLogin = true;
    }
 }

message: any;
durationInSeconds = 5;
hide: any;

openSnackBar(){
  this.hide = this.opcionEv[1].active === false ? 'Hide' : 'Quitar';
  this._snackBar.open(this.message, this.hide,  { 
    horizontalPosition: this.horizontalPosition,
    verticalPosition: this.verticalPosition,
    duration: this.durationInSeconds * 1000
  });
}
 
}
