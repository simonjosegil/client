import { AfterViewInit, Component, OnInit, ViewChild, OnDestroy, Output  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WebSocketService } from '../../services/web-socket.service';
import { LocalforageService, Item } from '../../services/localforage.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-bom',
  templateUrl: './bom.component.html',
  styleUrls: ['./bom.component.css']
})
export class BomComponent implements OnInit {
  
eventName = "send-message";
Res: any;
data:any;
b:any;
globalObject: any[] = [];
globalBuild: any[] = [];
BOM: any = [];
flag: any;
tachar = new Array;
x = 0;
partName = "";
partList = "";

loadBar: any;

userChat = {
  user: '',
  text: '',
  content: new Array,
  content2: new Array,
  type: 0,
  app: '',
  buildable: '',
  positionN: 0,
  positionJ: 0,
}

constructor(
  private activated : ActivatedRoute,
  private webService : WebSocketService, 
  private localStorage: LocalforageService
  
  ) 
{ this.clean();
  
  setTimeout(()=>{
    this.localStorage.get('USER').then((data:any) => {
      this.userChat.user = data;
  })},100)
}


ngOnInit(): void {
  const globalObject = [];
  this.noteTexBox = true;

  
  setTimeout(()=>{ this.noteTexBox = false; },10000)

  setTimeout(()=>{
    this.localStorage.get('opcionBom').then((data:any) => {
      this.opcionBom =  (data === null) || (data.length !== this.opcionBom.length) ? this.opcionBom : data;
      (data === null) || (data.length !== this.opcionBom.length) ? this.localStorage.set('opcionBom', this.opcionBom): null ;

    })},100)

    setTimeout(()=>{
      this.localStorage.get('Environment').then((data:any) => {
        this.opcionEv = data;
      })},300)

  this.webService.resp$.subscribe( respuesta =>
    {
      if(respuesta[0].type == 3){

      this.globalObject.push(respuesta)
      this.tachar = [{}];
      let promise = new Promise((resolve, reject) => 
      { 
        respuesta ? resolve(respuesta) : reject(new Error(''))
      }) 
      promise
      .then(()=>{
        this.localStorage.set('BOM', this.globalObject);
      })
      .then(()=>{
        this.localStorage.listKeys();
      })
      .then(()=>{
        setTimeout(()=>{
            this.localStorage.get('BOM').then(data => {
            this.BOM = data;           
            //console.log(this.BOM);
          })
            this.x = 1;
            this.loadBar = false;
            this.webService.loadBar$.emit(this.loadBar);
            setTimeout(()=>{this.int()},100)
            this.noteCompare = true;
            setTimeout(()=>{ this.noteCompare = false; },5000)
        },100)
        
      })
    }else if(respuesta[0].type == 6){
      this.globalBuild.push(respuesta[2].ALERT)      
      let promise = new Promise((resolve, reject) => 
      { 
        respuesta ? resolve(respuesta) : reject(new Error(''))
      }) 
      promise
      .then(()=>{
        setTimeout(()=>{
            this.BOM[respuesta[3].positionJ][1].build = respuesta[2].ALERT;           
            this.BOM[respuesta[3].positionJ][1].txtarea2 = respuesta[4].txtarea2;           

            this.x = 1;
            this.loadBar = false;
            this.webService.loadBar$.emit(this.loadBar);
        },100)
        
      })
    }else if(respuesta[0].type == 7){
      this.globalBuild.push(respuesta[2].ALERT)      
      let promise = new Promise((resolve, reject) => 
      { 
        respuesta ? resolve(respuesta) : reject(new Error(''))
      }) 
      promise
      .then(()=>{
        setTimeout(()=>{
          for (let i = 0; i < respuesta[2].ALERT.length; i++) {
            this.BOM[respuesta[3].positionJ][2].BOM[i].fPrice = respuesta[2].ALERT[i].description;
          }

          this.x = 1;
          this.loadBar = false;
          this.webService.loadBar$.emit(this.loadBar);
        },100)
        
      }) 
    }
    }) 
}

prueba: any;
selectedUser:any;
noteCompare = false;
noteTexBox = false;
opcionEv: any;
parts = false;


onParts(j:any){
  this.parts = this.parts === true ? false : true;
  //this.parts = true;
}

/* emitParts(){
  console.log(this.partName);
  console.log(this.partList);
} */


emitParts(j:any){
  this.loadBar=true;
  this.webService.loadBar$.emit(this.loadBar);
  this.userChat.type = 5;
  this.userChat.positionN = 17;
  this.userChat.text = this.partName.trim();
  this.userChat.app = this.BOM[j][1].name.trim().length === 11 ? 'mypim' : 'occ';
  this.userChat.positionJ = j;
  let arrPar = this.partList.split('\n');
  console.log(arrPar);
  let x;
  for (x of arrPar) {
    this.userChat.content.push(x.trim());
  }
  this.webService.emit(this.eventName, this.userChat);

  this.partName = "";
  this.partList = "";
  this.parts = false;
  this.cleanBuild();
}

selecCopy(j:any){
  let tt = 'testTable';
  let urlField: any = document.getElementById(tt.concat(j.toString()))
  const range = document.createRange();
  range.selectNodeContents(urlField);
  const sel: any = window.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
  document.execCommand('copy');   
}

opcionBom = [
  { name: 'Hide comparison guides ', nombre: 'Ocultar guías del comparador', active: true },
  { name: 'Only compare to 2nd place', nombre: 'Solo comparar al 2do puesto', active: false },
  { name: 'Do not compare', nombre: 'No comparar', active: false },
  { name: 'Hide Introduction(TextBox)', nombre: 'Ocultar Introducción(TextBox)', active: false },
  { name: 'ConfigID full description', nombre: 'Descripción completa(ConfigID)', active: false },
  { name: 'SAP Support', nombre: 'Soporte SAP', active: true },
  { name: 'Hide Date', nombre: 'Ocultar Fechas', active: true },
  { name: 'Hide Price', nombre: 'Ocultar Precios', active: true },
];

opcionBomStatus(){
  setTimeout(()=>{
  this.localStorage.set('opcionBom', this.opcionBom)
  this.getBomStatus();
  },100) 
}

getBomStatus(){
  setTimeout(()=>{
    this.localStorage.get('opcionBom').then((data:any) => {
           this.opcionBom =  data;
           //console.log(this.opcionBom);
  })},100)
}


inStyles = [
  { 'background-color': '#DAF7A6', 'background-clip': 'content-box'},
  { 'background-color': '#FEFBB1', 'background-clip': 'content-box'},
  { 'background-color': '#FEC7B1', 'background-clip': 'content-box'},
  { 'background-color': '#D4E6F1', 'background-clip': 'content-box'},
  { 'text-decoration': '', 'color': '' },
];

back(j: any){
let r = this.BOM[j];
let a = j-1;
let b = j+1;

this.BOM.splice(a, 0, r);
this.BOM.splice(b, 1);
this.globalObject = this.BOM;
this.int();
}

clearBom(j: any){
this.BOM.splice(j, 1);
this.globalObject = [];
this.localStorage.remove('BOM');

this.globalObject = this.BOM;
this.localStorage.set('BOM', this.BOM);
}

clearElemen(j: any, i: any){
  this.BOM[j][2].BOM.splice(i, 1);
  this.globalObject = [];
  this.localStorage.remove('BOM');
  this.globalObject = this.BOM;
  this.localStorage.set('BOM', this.BOM);
}

addElemen(j: any, i: any){
  let element =  { "av": "", "description": ""};
  this.BOM[j][2].BOM.splice(i, 0, element);
  this.globalObject = [];
  this.localStorage.remove('BOM');
  this.globalObject = this.BOM;
  this.localStorage.set('BOM', this.BOM);
}

appType = [
  { 'name': 'BOM', 'background-clip': 'content-box'},
  { 'name': 'BTO', 'background-clip': 'content-box'},
];

nameApp = "BOM"

editBto(j: any){
  this.nameApp = this.nameApp === "BOM" ? "BTO" : "BOM" ;
  this.parts = false;
}

int(){
if(this.BOM[1]){
    const arr1 = this.BOM[0][2].BOM;
    const arr2 = this.BOM[1][2].BOM;
    //La linea magica!!!
    const intersection = arr1.filter((item1:any) => arr2.some((item2:any) => item1.av === item2.av))

    this.compare(intersection, arr2)
    }
}		  

compare(inte:any, x:any){
for(let i =0; i < x.length;i++){
  if(this.opcionBom[1].active){  this.BOM[0][2].BOM[i].check = false; 
  }
  //if(this.opcionBom[1].active && this.BOM[2][2]){  this.BOM[2][2].BOM[i].check = false; }
  for(let j =0; j < inte.length;j++){
      if((x[i].av == inte[j].av) && !this.opcionBom[2].active){
        this.BOM[1][2].BOM[i].check = true;
           break; 
       }
  }
}
}

counter(j:any, i:any){
    if(this.BOM[j][2].BOM[i].status < this.inStyles.length && this.BOM[j][2].BOM[i].check != true){ 
      this.BOM[j][2].BOM[i].status = this.BOM[j][2].BOM[i].status+1;
    }else if(this.BOM[j][2].BOM[i].status = this.inStyles.length && this.BOM[j][2].BOM[i].check != true){
      this.BOM[j][2].BOM[i].status = 0;
    } 
}

clean(){
    this.globalObject = [];
    this.BOM = [];
    this.tachar = [{}];
    this.localStorage.remove('BOM');
    this.x = 0;
}

ele: any;

emitBuild(j:any){
    this.loadBar=true;
    this.webService.loadBar$.emit(this.loadBar);
    this.userChat.type = 5;
    this.userChat.positionN = 15;
    this.userChat.text = this.BOM[j][1].name;
    this.userChat.app = this.BOM[j][1].name.trim().length === 11 ? 'mypim' : 'occ';
    this.userChat.positionJ = j;
    let x;
    for (x of this.BOM[j][2].BOM) {
      this.userChat.content.push(x.av);
    }
    this.webService.emit(this.eventName, this.userChat);
    this.cleanBuild();
}


emitBOM2(j:any){
  this.loadBar=true;
  this.webService.loadBar$.emit(this.loadBar);
  this.userChat.type = 5;
  this.userChat.positionN = 17;
  this.userChat.text = this.BOM[j][1].name;
  this.userChat.app = this.BOM[j][1].name.trim().length === 11 ? 'mypim' : 'occ';
  this.userChat.positionJ = j;
  let x;
  for (x of this.BOM[j][2].BOM) {
    this.userChat.content.push(x.av);
  }
  this.webService.emit(this.eventName, this.userChat);
  this.cleanBuild();
}


emitPrice(j:any){
  this.loadBar=true;
  this.webService.loadBar$.emit(this.loadBar);
  this.userChat.type = 5;
  this.userChat.positionN = 19;
  this.userChat.text = this.BOM[j][1].name;
  this.userChat.app = this.BOM[j][1].name.trim().length === 11 ? 'mypim' : 'occ';
  this.userChat.positionJ = j;
  let x;
  for (x of this.BOM[j][2].BOM) {
    this.userChat.content.push(x.av);
  }
  this.webService.emit(this.eventName, this.userChat);
  this.cleanBuild();
}

saveBto(j: any){
    this.nameApp = this.nameApp === "BOM" ? "BTO" : "BOM" ;
    this.userChat.type = 5;
    this.userChat.positionN = 16;
    this.userChat.text = this.BOM[j][1].name;
    this.userChat.app = this.BOM[j][1].name.trim().length === 11 ? 'mypim' : 'occ';
    this.userChat.positionJ = j;
    let x;
    for (x of this.BOM[j][2].BOM) {
      this.userChat.content.push(x.av);
      this.userChat.content2.push(x.description);
    }
    this.webService.emit(this.eventName, this.userChat);
    this.cleanBuild();
}


cleanBuild(){
    this.userChat.text = '';
    this.userChat.content = new Array;
    this.userChat.content2 = new Array;
    this.userChat.type = 0;
    this.userChat.positionN = 0;
    this.loadBar = true;
    this.userChat.positionJ = 0;
  }

}

