import { ArrayType } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { empty } from 'rxjs';

import { WebSocketService } from '../../services/web-socket.service';
import { LocalforageService, Item } from '../../services/localforage.service';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

USER: any;
proUser: any;

userChat = {
  user: '',
  text: '',
  type: 3,
  positionN: 0,
  app: '',
  buildable: '',
}

credits = [{i:"Team Leader", o:"Diana Carolina Vergara"},
           {i:"Manager", o:"Carlos Ospina/Diego Rincón"},
           {i:"QA and Consultant", o:"Juliana Velasquez/Anderson Ortiz"},
           {i:"Consultant", o:"Fernando Higuita/Andres Toro"},
           {i:"Development test", o:"Jeferson Montenegro/Anderson Ortiz/Diego Penagos/Juliana Velasquez"},
           {i:"Test and Feedback", o:"Y.Castaño/D.Rincon/C.Ospina/C.Leal/S.Cardona/A.Restrepo/A.Alarcon/E.Arboleda/A.Tuiran/T.Santos"},
           {i:"Co-Author", o:"Fernando Higuita/Juliana Velasquez/Diego Penagos"},
           {i:"Author", o:"Simón Gil"},
           ];

myMessages: any;
Bom:  any;
eventName = "send-message";
Res: any;
data:any;
b:any;
credVar = false;

loadBar=false;

constructor(private activated : ActivatedRoute,
            private webService : WebSocketService, 
            private localStorage: LocalforageService,
            )
{  
setTimeout(()=>{
  this.localStorage.get('USER').then((data:any) => {
    this.userChat.user = data;
})},100)
}

onCredits(){
  this.credVar = this.credVar === true ? false : true;
}

ngOnInit(): void {
  this.webService.loadBar$.subscribe( resp => { this.loadBar=resp; })
  this.webService.listen('text-event').subscribe((data) => {
    this.myMessages = data;
  })
}

myMessage(){
  const position =  this.userChat.text.trim().length;
  this.getFunc(position);
}

getFunc(position:any) {
  const opt:any  = {
      6:  () => {},
      8:  () => {this.userChat.type = 3, this.userChat.positionN = 8, this.userChat.app = 'occ', this.goEmit()},
      11: () => {this.userChat.type = 3, this.userChat.positionN = 11, this.getDBom()}
  }  
  return opt[position] ? opt[position]() : this.errorComand();
}

 getDBom(){
  setTimeout(()=>{
    this.localStorage.get('opcionBom').then(async (data:any) => {
      let info = data;
      this.userChat.app = (data == null) || (data[5].active == false)  ? 'mypim': 'sap';
      console.log(info);
      //console.log(this.userChat);
      this.goEmit();
    })},100)
}

goEmit(){
  this.webService.emit(this.eventName, this.userChat);
  this.userChat.text = '';
  this.userChat.app = '';
  //this.userChat.type = 0;
  this.loadBar=true;
}

errorComand(){
  this.userChat.app = 'mypim';
  this.goEmit();
}

onSesion(){  
/*this.userChat.type = 0;
  this.webService.emit(this.eventName, this.userChat);
  this.userChat.type = 3;
  this.loadBar=true;  */
}

}


  //console.log(this.userChat.user);
  //const id = this.activated.snapshot.params.id;
  //this.userChat.user = id;