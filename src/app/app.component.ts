import { Component, OnInit } from '@angular/core';
import { WebSocketService } from './services/web-socket.service';
import { LocalforageService, Item } from './services/localforage.service';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Presales Online';
 
  userChat = {
    user: '',
    text: '',
    type: 0
  }
  
  eventName = "send-message";
  data:any;

  environment: any;

  constructor(
    private webService : WebSocketService, 
    private localStorage: LocalforageService
 ) { }

 ngOnInit(): void {

  this.iniEv();
   
}

opcionEv = [
  { name: 'Hide automated browser', nombre: 'Ocultar navegador automatizado', active: false },
  { name: 'Spanish Language', nombre: 'Idioma Español',  active: false },
  { name: 'Login from mobile App "PingID" ', nombre: 'Ingreso desde la App móvil "PingID"',  active: false },
  { name: 'Optional input method', nombre: 'Metodo de ingreso opcional',  active: false, method: 'Default', mail:'', password: ''},

];

iniEv(){

  setTimeout(()=>{
    this.localStorage.get('Environment').then((data:any) => {
      this.userChat.text = data === null ? this.opcionEv : data;
      (data === null) || (data.length !== this.opcionEv.length) ?  this.localStorage.set('Environment', this.opcionEv) : null;
      console.log(data.length);
      
      this.webService.emit('send-message', this.userChat);
      this.userChat.text = '';
     })
    },100) 
}

restore(){
  this.localStorage.remove('Environment');
  setTimeout(()=>{ window.location.reload(); },3000) 
}

}
