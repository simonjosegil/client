import { Injectable, EventEmitter } from '@angular/core';
import { io } from 'socket.io-client'
import { from, Observable, Subscriber } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  socket : any;
  server = "http://localhost:3000/"
 
  Res: any;
  resp$ = new EventEmitter<any>();
  loadBar$ = new EventEmitter<any>();


  constructor() { 
    this.socket =  io(this.server, { transports: ['websocket'] } );
  }
  listen(eventName: String){
    return new Observable((Subscriber) =>{
      this.socket.on(eventName, (data: any)  =>{
        Subscriber.next(data);
      })
    })
  }

emit(eventName: String, data: any){
  let x = this;

  this.socket.emit(eventName, data, function( resp: any ){
    console.log(resp);
    x.resp$.emit(resp.resp);
  })
}
}



