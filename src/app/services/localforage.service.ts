import { Injectable } from '@angular/core';
import * as localforage from 'localforage'; 

export interface Item {
  respuesta: []
}

const ITEMS_KEY = 'BOM';

@Injectable({
  providedIn: 'root'
})



export class LocalforageService {

  constructor() {
    localforage.config({
      name: 'App Storage'
    });
  }

  addItem(respuesta:any): Promise<any> {
    return this.get(ITEMS_KEY).then((respuesta:any) => {
      if (respuesta) {
        respuesta.push(respuesta);
        return this.set(ITEMS_KEY, respuesta);
      } else {
        return this.set(ITEMS_KEY, respuesta);
      }
    });
  }

  get(key: any) {
    return localforage.getItem(key);
  }

  set(key: any, value: any) {
    return localforage.setItem(key, value);
  }

  remove(key: any) {
    return localforage.removeItem(key);
  }

  DELETE_ALL() {
    return localforage.clear();
  }

  listKeys() {
    return localforage.keys();
  }

}